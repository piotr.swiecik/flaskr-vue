"""
Security-related.
"""
import functools
from flask import jsonify, request, abort, current_app
from itsdangerous import URLSafeTimedSerializer
from . import restapi
import flask.wrappers
from ..dbmodels import User


def login_required(viewfunction):
    """
    Login required decorator.
    For now - placeholder. Will implement JWT logic.
    """
    @functools.wraps(viewfunction)
    def decorated(*args, **kwargs):
        # does nothing yet!
        return viewfunction(*args, **kwargs)
    return decorated


@restapi.route('/auth', methods=['POST'])
def auth_user() -> flask.wrappers.Response:
    """
    Check POST request payload against database.
    Payload contains username & plaintext password.
    If user OK - return auth token.
    If user not OK - return failed status (user_id=-999)
    """
    # for now - mock return value

    req_data = request.get_json()

    if not (req_data['username'] and req_data['password']):
        current_app.logger.error(f'Login request with missing data.')
        abort(400)

    user = User.get_by_username(req_data['username'])

    if user:
        if user.password_correct(req_data['password']):
            current_app.logger.info(f'User {user.username} logged in.')
            return jsonify({
                'auth_token': '12345',  # placeholder for now
                'username': user.username,
                'user_id': user.id,
            })

    # if authentication failed (no username or no password - same outcome)
    current_app.logger.error(f'User {req_data} failed to log in.')
    return jsonify({
        'auth_token': None,
        'username': None,
        'user_id': -999,
    })