"""
REST API endpoints related to blog posts.
"""
import datetime
import flask.wrappers
import functools
from typing import Tuple
from flask import jsonify, abort, request
from . import restapi
from sqlalchemy import desc
from flaskr.dbmodels import Post, User
from flaskr.serialize import PostSchema
from flaskr.restapi.security import login_required
from sqlalchemy.exc import SQLAlchemyError


@restapi.route('/posts', methods=['GET'])
def get_posts() -> Tuple[flask.wrappers.Response, int]:
    """
    Serialize all posts existing in database and return REST API response.
    :return: Flask response object. Contains all posts as list wrapped in JSON.
    """
    result = Post.query.order_by(desc(Post.date_created)).all()
    result_schema = PostSchema(many=True)
    result_serialized = result_schema.dump(result)
    current = 0
    # include author's username in serialization
    for post in result:
        user = User.query.get(post.author_id)
        result_serialized[current]['author'] = user.username
        current += 1
    # include number of likes in serialization
    [res.update(num_likes=len(post.likes)) for post in result for res in result_serialized]
    # include number of comments in serialization
    [res.update(num_comments=len(post.comments)) for post in result for res in result_serialized] # todo looks wrong
    return jsonify(result_serialized), 200


@restapi.route('/post/<int:_id>', methods=['GET'])
def get_post(_id: int) -> Tuple[flask.wrappers.Response, int]:
    """
    Serialize single post with given ID matching DB entry and return REST API response.
    :return: Flask response object. Contains single post as JSON.
    """
    result = Post.query.get(_id)
    if result is None:
        return jsonify({'message': 'Post with this ID was not found'}), 404

    # manual processing of Marshmallow serialization result
    # alternative is to use @pre_dump decorator - but this means problems with app context & imports
    result_schema = PostSchema()
    result_serialized = result_schema.dump(result)
    # add likes count to serialization dict
    result_serialized['likes'] = len(result.likes)  # result.likes is sqlalchemy.orm.collections.InstrumentedList
    # add username
    user = User.query.get(result.id)
    result_serialized['author'] = user.username
    # add tag keywords as list to serialization dict
    result_serialized['tag_keywords'] = [tag.keyword for tag in result.tags]
    # add individual comments - each comment object contains comment body, author and date
    result_serialized['comment_content'] = [
        {'body': comment.body, 'author': User.query.get(comment.author_id).username, 'date': str(comment.date_created)}
        for comment in result.comments
    ]

    return jsonify(result_serialized), 200


@restapi.route('/posts', methods=['POST'])
@login_required
def new_post() -> Tuple[flask.wrappers.Response, int]:
    """
    Obtains new post data from request body and creates post in database.
    Authorization required.
    """
    deserialized = request.get_json()
    if not all([deserialized['title'], deserialized['author_id']]):
        return jsonify({'message': 'Data fields missing in request'}), 400

    post = Post(title=deserialized['title'],
                body=deserialized['body'],
                author_id=deserialized['author_id'],
                tags=deserialized['tags'])

    try:
        post.save_to_db()
        return jsonify({'message': 'post added to database'}), 201
    except SQLAlchemyError:
        return jsonify({'message': 'Database internal error'}), 500
