"""
Package flaskr.restapi: top-level package for flaskr REST.
Defines REST API blueprint.
"""
from flask import Blueprint, current_app, request

restapi = Blueprint('restapi', __name__)

from . import posts, users, comments, errors


@restapi.before_request
def dev_log_request():
    current_app.logger.debug(f'API request received: [{request.method}]{ request.path}')

