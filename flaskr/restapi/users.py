"""
REST API endpoints related to users.
"""
import flask.wrappers
from typing import Tuple
from flask import jsonify
from . import restapi
from flaskr.dbmodels import User
from flaskr.serialize import UserSchema


@restapi.route('/users/<int:_id>', methods=['GET'])
def get_user(_id: int) -> Tuple[flask.wrappers.Response, int]:
    """
    Serialize data of single given user - excluding hashed password - and return REST API response.
    :param _id: user ID to match Postgres DB entry.
    :return: Flask response object.
    """
    result = User.query.get(_id)
    if result:
        result_schema = UserSchema()
        # serialize using marshmallow schema
        # then append additional keys as needed to 'result_serialized'
        result_serialized = result_schema.dump(result)
        # finally - wrap & return a response
        return jsonify(result_serialized), 200



