"""
Custom error handlers.
"""
from . import restapi
from flask import jsonify


@restapi.errorhandler(400)
def bad_request(err):
    return jsonify({
        'status': 'Error',
        'code': 400,
        'message': 'Bad request'
    })
