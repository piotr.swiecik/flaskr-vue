"""
Location of main db object.
ORM mapper classes - db model definitions.
"""
import datetime
from typing import List, Any, Optional
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData
from sqlalchemy.orm import validates
from flask_migrate import Migrate
from werkzeug.security import generate_password_hash, check_password_hash

convention = {  # key naming conventions to be used in SQLAlchemy metadata
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=convention)
db = SQLAlchemy(metadata=metadata)  # to be imported and used by app factory
migrate = Migrate(db)


class User(db.Model):
    """
    Represents user account in postgres database.
    """

    __tablename__ = 'users'  # avoid postgres issues with 'user' builtin

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String(320), unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)  # hashed
    email_confirmed = db.Column(db.Boolean, nullable=False, default=False)
    activated = db.Column(db.Boolean, nullable=False, default=False)  # activation by admin
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    date_registered = db.Column(db.DateTime)
    date_confirmed = db.Column(db.DateTime)
    date_activated = db.Column(db.DateTime)

    def __init__(self,
                 username: str,
                 email: str,
                 password_plaintext: str,
                 is_admin: bool = False):
        self.username = username
        self.email = email
        self.password = generate_password_hash(password_plaintext)
        self.is_admin = is_admin
        self.date_registered = datetime.datetime.now()

    @classmethod
    def get_by_username(cls, username: str) -> Optional['User']:
        """
        Query DB for given username, return User or None.
        """
        return cls.query.filter_by(username=username).first()

    @classmethod
    def get_by_id(cls, _id: int) -> Optional['User']:
        """
        Query DB for given user id, return User or None.
        """
        return cls.query.get(_id)

    def password_correct(self, plaintext_pass: str) -> bool:
        """
        Check password against password hash for this User object.
        """
        return check_password_hash(self.password, plaintext_pass)

    @validates('email')
    def validate_email(self, key, value):
        """
        Provided address must contain '@'.
        """
        if '@' not in value:
            raise ValueError('Email address validation failed!')
        return value

    @validates('username')
    def validate_username(self, key, value):
        """
        Provided username must be 4-64 characters long.
        """
        if len(value) < 4 or len(value) > 64:
            raise ValueError('Username validation failed!')
        return value

    def __repr__(self):
        return f'User {self.id}: {self.username}'


# join table for tag-post relationship
tags_table = db.Table('tags',
                      db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'), primary_key=True),
                      db.Column('post_id', db.Integer, db.ForeignKey('post.id'), primary_key=True)
                      )

# join table for likes
likes_table = db.Table('likes',
                       db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True),
                       db.Column('post_id', db.Integer, db.ForeignKey('post.id'), primary_key=True)
                       )


class Post(db.Model):
    """
    Represents blog post in postgres database.
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    body = db.Column(db.Text, nullable=False)
    date_created = db.Column(db.DateTime)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    # lazy='dynamic' makes tags return an AppenderBaseQuery
    # if lazy='joined' is used, then output is a list of tags
    tags = db.relationship('Tag', secondary=tags_table, backref=db.backref('posts', lazy=True), lazy='dynamic')

    # todo - verify lazy load settings - optimize & test
    likes = db.relationship('User', secondary=likes_table, backref=db.backref('liked', lazy=True), lazy='joined')
    comments = db.relationship('Comment', backref='post', lazy='joined')

    def __init__(self,
                 title: str,
                 body: str,
                 author_id: int,
                 tags: Optional[List[str]],
                 ):

        self.title = title
        self.body = body
        self.author_id = author_id  # validated separately as validate_author_id()
        self.date_created = datetime.datetime.now()

        if tags:  # todo test if this works correctly
            for tag in tags:
                result = Tag.query.filter_by(keyword=tag).first()
                if result is None:
                    raise ValueError('Invalid tag!')  # todo modify this so new tags are created on the fly
                else:
                    self.tags.append(result)

    def __repr__(self):
        result = User.query.filter_by(id=self.author_id).first()  # match username to author id
        return f'Post {self.id} by {result.username} on {self.date_created}'

    @validates('author_id')
    def validate_author_id(self, key, value: int) -> int:
        result = User.query.filter_by(id=value).first()
        if result is None:
            raise ValueError('Invalid author ID!')
        return value

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class Tag(db.Model):
    """
    Represents a post tag in postgres database.
    Related many (tags) to many (posts).
    """
    # posts related to tag available via 'posts' backref
    id = db.Column(db.Integer, primary_key=True)
    keyword = db.Column(db.String(30), nullable=False, unique=True)

    def __init__(self, keyword: str):
        self.keyword = keyword

    def __repr__(self):
        return f'Tag {self.id}: {self.keyword}'

    @validates('keyword')
    def validate_keyword(self, key, value):
        if len(value) < 1:
            raise ValueError('Empty tags not allowed!')
        return value


class Comment(db.Model):
    """
    Represents a comment in postgres database.
    Related many (comments) to one (post).
    """
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), nullable=False)
    body = db.Column(db.String(300))
    date_created = db.Column(db.DateTime)

    def __init__(self,
                 author_id: int,
                 post_id: int,
                 body: str,
                 ):
        self.author_id = author_id
        self.post_id = post_id
        self.body = body
        self.date_created = datetime.datetime.now()

    def __repr__(self) -> str:
        result = User.query.filter_by(id=self.author_id).first()
        return f'Comment {self.id} by {result.username} @ {self.date_created}, parent post {self.post_id}'

    @validates('post_id')
    def validate_parent_post_id(self, key, value: int) -> int:
        # verify if post given in constructor exists
        result = Post.query.filter_by(id=value).first()
        if result is None:
            raise ValueError('Parent post does not exist!')
        return value

    @validates('author_id')
    def validate_author_id(self, key, value: int) -> int:
        # verify if author id given in constructor exists
        result = User.query.filter_by(id=value).first()
        if result is None:
            raise ValueError('User id does not exist!')
        return value

# todo post schema - nested with tags, comments, author etc.
