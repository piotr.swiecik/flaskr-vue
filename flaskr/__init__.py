"""
Main package. Contains app factory with all initialization routines.
"""
import os
import logging
from logging.handlers import RotatingFileHandler
from typing import Any, Optional, List, Mapping
from flask import Flask
from flask.logging import default_handler
from flask_migrate import migrate
from flask_cors import CORS


def create_app(custom_config: Optional[Mapping[str, Any]] = None) -> Flask:
    """
    :param custom_config: manually provide optional config settings as dict - override defaults.
    :return: configured Flask app
    """
    app = Flask(__name__, instance_relative_config=False)  # config file is in root dir, not in instance

    # config initialization
    if custom_config is None:  # if no dict, then parse defaultconfig.py depending on env var FLASK_ENV
        app.config.from_object(
            ''.join(['defaultconfig',
                     '.',
                     'Config',
                     os.getenv('FLASK_ENV', default='production').title()])
        )
    else:  # if dict, then parse dictionary given as parameter and override defaults
        app.config.from_mapping(custom_config)

    # logging initialization
    # set up a rotating log file for given environment and deactivate default logger
    # log files live in instance folder
    # loglevel depends on environment

    log_handler = RotatingFileHandler(
        os.path.join('instance', 'logs', ''.join(['flaskr_', os.getenv('FLASK_ENV', default='production'), '.log'])),
        maxBytes=2**14,
        backupCount=5
    )

    log_formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(filename)s:%(lineno)d]')
    log_handler.setFormatter(log_formatter)
    match os.getenv('FLASK_ENV', default='production'):
        case 'development':
            log_handler.setLevel(logging.DEBUG)
        case 'testing':
            log_handler.setLevel(logging.DEBUG)
        case _:
            log_handler.setLevel(logging.ERROR)

    app.logger.addHandler(log_handler)
    app.logger.removeHandler(default_handler)

    # database initialization section
    from .dbmodels import db  # SQLAlchemy object created in same module as ORM definitions
    from .dbmodels import migrate  # Alembic wrapper
    db.init_app(app)  # initialize before using in app context - responsible mainly for teardown / connection close
    migrate.init_app(app, db)  # initialize Alembic migration module

    # serialization init
    from .serialize import ma
    ma.init_app(app)  # important - Marshmallow must be initialized after SQLAlchemy

    # CORS
    CORS(app)  # todo development only - allows CORS on all routes

    # blueprint section
    from flaskr.restapi import restapi as restapi_blueprint
    app.register_blueprint(restapi_blueprint, url_prefix='/api')

    app.logger.info(f'App instance starting in environment: {os.getenv("FLASK_ENV")}')
    return app


