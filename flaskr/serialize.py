"""
Marshmallow serialization schemas.
"""
from flask import current_app
from flask_marshmallow import Marshmallow
from marshmallow import pre_dump
from flaskr.dbmodels import User, Post, Comment, Tag

ma = Marshmallow()


class UserSchema(ma.SQLAlchemySchema):
    class Meta:
        model = User

    # password field is not serialized
    id = ma.auto_field()
    username = ma.auto_field()
    email = ma.auto_field()
    is_admin = ma.auto_field()
    email_confirmed = ma.auto_field()
    activated = ma.auto_field()
    date_registered = ma.auto_field()


class PostSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Post

    id = ma.auto_field()
    title = ma.auto_field()
    body = ma.auto_field()
    date_created = ma.auto_field()
    author_id = ma.auto_field()
    tags = ma.auto_field()  # only tag ids - keywords are serialized manually
    comments = ma.auto_field()  # same as above
