// vuex auth module
import authGetters from './getters';
import authActions from './actions';
import authMutations from './mutations';

const authModule = {
    namespaced: true,
    state() {
        return {
            userId: null,
            authToken: null,
            authTokenExpiration: null,
            tokenExpiresIn: 0,
            userIsAuthenticated: false,
            activeUserName: '',
            tokenExpiryTime: 3600, // todo - move this to backend
        };
    },
    getters: authGetters,
    mutations: authMutations,
    actions: authActions,
};

export default authModule;
