// vuex getters related to auth module
export default {
    getUserIsAuthenticated(state) {
        return state.userIsAuthenticated;
    },
};
