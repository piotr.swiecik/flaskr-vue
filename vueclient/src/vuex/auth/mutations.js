// vuex mutations related to auth module
export default {
    setUserAuthenticated(state, payload) {
        // utility method called from loginUserAction after authentication against backend was successful
        // sets authenticated state to true & stores session data including token in vuex state manager
        state.userIsAuthenticated = true;
        state.activeUserName = payload.username;
        state.tokenExpiresIn = new Date().getTime() + state.tokenExpiryTime * 1000;
        state.userId = payload.userId;
        state.authToken = payload.authToken;
        localStorage.setItem('authToken', state.authToken);
        localStorage.setItem('userId', state.userId);
        localStorage.setItem('activeUserName', state.activeUserName);
    },
    setUserNotAuthenticated(state) {
        // utility method - toggles userIsAuthenticated=false
        state.userIsAuthenticated = false;
        state.tokenExpiresIn = 0;
        localStorage.removeItem('authToken');
        localStorage.removeItem('userId');
        localStorage.removeItem('activeUserName');
    }
};
