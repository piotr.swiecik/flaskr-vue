// vuex actions related to auth module
export default {
    async loginUserAction(context, payload) {
        // main login handler - initiated via BaseModalForm 'submit-login' event
        // payload must contain 'username' and 'password'
        // 1. perform authentication via REST API - obtain token
        // 2. if auth successful - store token and user data in local store + set expiration timer ('setUserAuthenticated' mutation)

        const url = 'http://localhost:5000/api/auth'; // todo url from env var

        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                username: payload.username,
                password: payload.password,
            }),
            headers: {
                'Content-Type': 'application/json',
            }
        });

        // by API spec:
        // auth_token: token
        // username: username or ''
        // user_id: id or -999 if login invalid
        const responseData = await response.json();

        console.log('auth response:');
        console.log(responseData);

        if (!response.ok) {
            throw new Error(responseData.message || 'Authentication error!');
        }

        if (responseData.auth_token && responseData.user_id > 0) {
            await context.commit({
                type: 'setUserAuthenticated',
                username: responseData.username,
                authToken: responseData.auth_token,
                userId: responseData.user_id,
            });
        } else {
            throw new Error('Invalid login credentials!');
        }
    },
    logoutUserAction(context) {
        // main logout handler - initiated via logout @click from TheHeader
        context.commit('setUserNotAuthenticated');
    },
};
