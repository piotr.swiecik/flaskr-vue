// top level vuex vuex
import { createStore } from "vuex";
import authModule from "@/vuex/auth";
import blogModule from "@/vuex/blog";

const store = createStore({
    modules: {
        auth: authModule,
        blog: blogModule,
    },
    state() {
        return {};
    },
});

export default store;
