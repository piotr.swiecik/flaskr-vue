export default {
    setPaginationBatchSize(state, payload) {
        state.paginationBatchSize = payload.paginationBatchSize;
    },
    setPaginationBatchCurrent(state, payload) {
        state.paginationBatchCurrent = payload.paginationStep;
    }
};
