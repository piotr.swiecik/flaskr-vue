export default {
    getPaginatedPosts(state) {
        // number of returned posts is based on state variables: paginationBatchSize, paginationBatchCurrent
        let paginationLoad = state.paginationBatchSize * (state.paginationBatchCurrent + 1);
        const maxPaginationLoad = state.posts.length;
        if (paginationLoad > maxPaginationLoad) {
            paginationLoad = maxPaginationLoad;
        }
        return state.posts.slice(0, paginationLoad);
    },
    getMaxPosts(state) {
        return state.posts.length;
    }
};
