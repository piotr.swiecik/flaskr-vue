// vuex blog module
import blogGetters from './getters';
import blogActions from './actions';
import blogMutations from './mutations';

const blogModule = {
    namespaced: true,
    state() {
        return {
            // todo refactor dummy blog posts into REST interface
            // format defined in REST
            posts: [
                {
                    "author": "piotr",
                    "author_id": 1,
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "comments": [
                        1
                    ],
                    "num_comments": 0,
                    "date_created": "2022-06-10T16:49:36.507823",
                    "id": 1,
                    "num_likes": 0,
                    "tags": [
                        1
                    ],
                    "title": "First Test Post"
                },
                {
                    "author": "piotr",
                    "author_id": 1,
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "comments": [
                        1
                    ],
                    "num_comments": 5,
                    "date_created": "2022-06-10T16:49:36.507823",
                    "id": 2,
                    "num_likes": 0,
                    "tags": [
                        1
                    ],
                    "title": "Second Test Post"
                },
                {
                    "author": "piotr",
                    "author_id": 1,
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "comments": [
                        1
                    ],
                    "num_comments": 0,
                    "date_created": "2022-06-10T16:49:36.507823",
                    "id": 3,
                    "num_likes": 0,
                    "tags": [
                        1
                    ],
                    "title": "testtile"
                },
                {
                    "author": "piotr",
                    "author_id": 1,
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "comments": [
                        1
                    ],
                    "num_comments": 0,
                    "date_created": "2022-06-10T16:49:36.507823",
                    "id": 4,
                    "num_likes": 0,
                    "tags": [
                        1
                    ],
                    "title": "testtile"
                },
                {
                    "author": "piotr",
                    "author_id": 1,
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "comments": [
                        1
                    ],
                    "num_comments": 0,
                    "date_created": "2022-06-10T16:49:36.507823",
                    "id": 5,
                    "num_likes": 0,
                    "tags": [
                        1
                    ],
                    "title": "testtile"
                },
            ],
            paginationBatchSize: 5,
            paginationBatchCurrent: 0,
        };
    },
    getters: blogGetters,
    actions: blogActions,
    mutations: blogMutations,
};

export default blogModule;
