import { createApp } from 'vue'
import App from './App.vue'
import router from './router';
import store from './vuex/index';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import BaseModalForm from "@/components/ui/BaseModalForm";
import BaseModalError from "@/components/ui/BaseModalError";
import BaseButton from "@/components/ui/BaseButton";

const app = createApp(App);
app.use(router);
app.use(store);

app.component('base-modal-form', BaseModalForm);
app.component('base-button', BaseButton);
app.component('base-modal-error', BaseModalError);

app.mount('#app');
