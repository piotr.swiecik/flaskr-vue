import { createRouter, createWebHistory } from "vue-router";
import PostTimeline from "@/pages/PostTimeline";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: '/blog',
        },
        {
            name: 'timeline',
            path: '/blog',
            component: PostTimeline,
        }
    ],
});

export default router;
