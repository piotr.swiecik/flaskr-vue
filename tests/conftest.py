"""
Implementation of pytest fixtures.
Execute as python -m pytest -vv to avoid module cross-import errors.
"""
import pytest
import os
from flaskr import create_app
from flaskr.dbmodels import db
from flaskr.dbmodels import User, Post, Tag, Comment


@pytest.fixture(scope='function')
def app():
    """
    Provides a fully configured Flask app instance for testing.
    :return: Flask app.
    """
    # todo database init
    if os.getenv('FLASK_ENV') != 'testing':
        raise Exception('Not in testing environment! Please export FLASK_ENV=testing first.')
    app = create_app()

    # database creation
    with app.app_context():
        db.create_all()  # create schema

        # create test user with no admin access
        testuser = User(username='testuser',
                        email='testuser@testuser.com',
                        password_plaintext='testuser',
                        is_admin=False)

        # create another test user with no admin access
        testuser_2 = User(username='anotheruser',
                          email='anotheruser@anotheruser.com',
                          password_plaintext='anotheruser',
                          is_admin=False)

        db.session.add_all([testuser, testuser_2])
        db.session.commit()

        # create tags
        testtag_a = Tag(keyword='test tag')
        testtag_b = Tag(keyword='another tag')
        db.session.add_all([testtag_a, testtag_b])
        db.session.commit()

        author = User.query.filter_by(username='testuser').first()
        commenter = User.query.filter_by(username='anotheruser').first()

        # create test post
        testpost = Post(title='Test title',
                        body='Test post body',
                        author_id=author.id,
                        tags=['test tag', 'another tag'])

        db.session.add(testpost)
        db.session.commit()

        # create comment
        testpost = Post.query.filter_by(title='Test title').first()
        testcomment = Comment(author_id=commenter.id,
                              post_id=testpost.id,
                              body='A test comment')
        db.session.add(testcomment)
        db.session.commit()

    yield app

    # database cleanup
    with app.app_context():
        db.drop_all()


@pytest.fixture(scope='function')
def client(app):
    """
    Provides a HTTP test client.
    :param app: Flask app.
    :return: FlaskClient instance.
    """
    return app.test_client()

