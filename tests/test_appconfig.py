"""
Tests related to basic app factory & app config.
"""


def test_app_status(app, client) -> None:
    """
    Test if config is properly loaded - with test environment variables, test database etc.
    """
    assert app.config['DEBUG'] is False
    assert app.config['TESTING'] is True
    assert app.config['ENV'] == 'testing'
    assert app.config['SECRET_KEY'] == 'TEST_PLACEHOLDER_SECRET_KEY'
    response = client.get('/this/route/does/not/exist')
    assert response.status_code == 404


def test_database_selftest(app) -> None:
    """
    Check if psql test database is correctly initialized.
    """
    # todo - creation dates are not tested
    from flaskr.dbmodels import db, User, Post
    from werkzeug.security import check_password_hash
    # verify test user(s)
    with app.app_context():
        user_result = User.query.filter_by(username='testuser').first()
        commenter_result = User.query.filter_by(username='anotheruser').first()
        assert user_result is not None
        assert commenter_result is not None
        assert user_result.username == 'testuser'
        assert user_result.email == 'testuser@testuser.com'
        assert check_password_hash(user_result.password, 'testuser')
        assert user_result.is_admin is False
    # verify test post(s)
    with app.app_context():
        post_result = Post.query.filter_by(title="Test title").first()
        assert post_result is not None
        assert post_result.title == 'Test title'
        assert post_result.body == 'Test post body'
        assert post_result.author_id == user_result.id
        assert post_result.tags[0].keyword == 'test tag'  # AppenderBaseQuery object - because lazy='dynamic' in ORM
        assert post_result.tags[1].keyword == 'another tag'
        assert post_result.comments[0].body == 'A test comment'  # AppenderBaseQuery object
        assert post_result.comments[0].author_id == commenter_result.id

