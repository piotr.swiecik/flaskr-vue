"""
Tests related to post blueprint.
"""
import datetime


def test_post_serialization(app, client):
    """
    Given a test app with pre-initialized database.
    When test client requests (GET) a post by id via '/api/posts/<id>/'.
    Then REST API response contains correct serialized data.
    """
    response = client.get('/api/posts/1/')
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    deserialized = response.get_json()
    assert deserialized['title'] == 'Test title'
    assert deserialized['body'] == 'Test post body'
    assert 'test tag' in deserialized['tag_keywords']
    assert 'another tag' in deserialized['tag_keywords']
    assert str(datetime.date.today()) in deserialized['date_created']
    assert deserialized['comment_content'] is not None
    assert deserialized['comment_content'][0]['author'] == 'anotheruser'
    assert deserialized['comment_content'][0]['body'] == 'A test comment'
    assert str(datetime.date.today()) in deserialized['comment_content'][0]['date']
    assert deserialized['author'] == 'testuser'
