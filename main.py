from flaskr import create_app
from flaskr.dbmodels import db, User, Post, Comment, Tag
from flaskr.serialize import UserSchema, PostSchema

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'User': User,
        'Post': Post,
        'Comment': Comment,
        'Tag': Tag,
        'UserSchema': UserSchema,
        'PostSchema': PostSchema
    }
