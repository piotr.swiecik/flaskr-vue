"""
Default config settings from PROD, DEV, TEST environments.
Should be located in root dir (instance_relative_config=False).
"""
import os


class ConfigParent:
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ConfigProduction(ConfigParent):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.getenv('SECRET_KEY')  # no default for PROD environment


class ConfigDevelopment(ConfigParent):
    DEBUG = True
    TESTING = False
    SECRET_KEY = 'DEV_PLACEHOLDER_SECRET_KEY'


class ConfigTesting(ConfigParent):
    DEBUG = False
    TESTING = True
    SECRET_KEY = 'TEST_PLACEHOLDER_SECRET_KEY'
    SQLALCHEMY_DATABASE_URI = os.getenv('TEST_DATABASE_URL')
